#pragma once

#include <QtGui/QGuiApplication>

class QSharedMemory;

class SingleApplication final : public QGuiApplication
{
    Q_OBJECT

public:
    SingleApplication(int& argc, char** argv, QString key);

    bool isRunning() const;

private:
    Q_DISABLE_COPY(SingleApplication)

private:
    QSharedMemory* sharedMemory_;
    QString key_;
    bool isRunning_;
};
