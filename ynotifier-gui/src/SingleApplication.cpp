#include <QtCore/QDebug>
#include <QtCore/QSharedMemory>

#include "SingleApplication.h"

SingleApplication::SingleApplication(int &argc, char **argv, QString key)
    : QGuiApplication(argc, argv)
    , sharedMemory_(new QSharedMemory(this))
    , key_(std::move(key))
{
    sharedMemory_->setKey(key_);

    if (sharedMemory_->attach()) {
        isRunning_ = true;
    } else {
        isRunning_ = false;

        if (!sharedMemory_->create(1)) {
            qCritical() << sharedMemory_->errorString();
            exit(1);
        }
    }
}

bool SingleApplication::isRunning() const
{
    return isRunning_;
}
