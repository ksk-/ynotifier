#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>

#include "NotificationModel.h"
#include "NotificationReceiver.h"
#include "SingleApplication.h"

int main(int argc, char** argv)
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    SingleApplication app(argc, argv, APPLICATION_NAME);


    if (app.isRunning()) {
        return 0;
    }

    NotificationModel model;
    NotificationReceiver reciver(model);

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty(QStringLiteral("notificationModel"), &model);
    engine.load(QStringLiteral("qrc:/src/qml/main.qml"));

    reciver.start();

    return app.exec();
}
