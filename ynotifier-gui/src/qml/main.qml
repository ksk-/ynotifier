import QtQuick 2.6
import QtQuick.Controls 2.0
import QtQuick.Window 2.0

ApplicationWindow {
    id: root

    property int margin: 10

    flags: Qt.ToolTip | Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint | Qt.WA_TranslucentBackground
    color: "transparent"
    visible: true

    width: 300
    height: Math.min(view.contentHeight, Screen.desktopAvailableHeight - 2 * margin)

    x: Screen.desktopAvailableWidth - width - margin
    y: Screen.desktopAvailableHeight - height - margin

    ListView {
        id: view

        property int notificationHeight: 80
        property int contentHeight: notificationHeight * Math.max(model.count, 1) + spacing * (model.count - 1)

        anchors.fill: parent
        anchors.leftMargin: 2
        anchors.rightMargin: 2

        model: notificationModel

        delegate: Notifiaction {
            id: delegate
        }

        spacing: 10

        Connections {
            target: notificationModel; onCountChanged: {
                if (notificationModel.count === 0) {
                    root.close()
                }
            }
        }
    }
}
