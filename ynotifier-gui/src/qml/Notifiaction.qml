import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

import QtGraphicalEffects 1.12

Rectangle {
    width: parent.width
    height: view.notificationHeight

    color: "white"
    border.color: "gray"

    ColumnLayout {
        width: parent.width

        RowLayout {
            Text {
                Layout.leftMargin: 5
                Layout.fillWidth: true

                text: "<b>" + title + "</b>"
            }

            Button {
                Layout.preferredWidth: 20
                Layout.preferredHeight: 20
                Layout.margins: 2

                flat: true

                text: "X"

                onClicked: {
                    view.model.remove(index)
                }
            }
        }

        Text {
            Layout.leftMargin: 5
            text: content
        }
    }

    layer.enabled: true
    layer.effect: Glow {
        spread: 0.2
        color: border.color
        transparentBorder: true
    }
}
