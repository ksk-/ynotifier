#include <QtCore/QDebug>
#include <QtCore/QJsonDocument>

#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>

#include "NotificationModel.h"

#include "NotificationReceiver.h"

constexpr auto DEFAULT_PORT = 3333;

namespace
{
    QByteArray readBase64Line(QIODevice& device)
    {
        return QByteArray::fromBase64(device.readLine().trimmed());
    }
}

NotificationReceiver::NotificationReceiver(NotificationModel& model, QObject* parent)
    : QObject(parent)
    , model_(model)
    , server_(new QTcpServer(this))
{}

void NotificationReceiver::start()
{
    connect(server_, &QTcpServer::newConnection, [this] {
        QTcpSocket* socket = server_->nextPendingConnection();
        Q_CHECK_PTR(socket);

        connect(socket, &QIODevice::readyRead, [this, socket] {
            while (socket->canReadLine()) {
                model_.add(QJsonDocument::fromJson(readBase64Line(*socket)).object());
            }
        });

        connect(socket, &QAbstractSocket::disconnected, socket, &QObject::deleteLater);
    });

    if (server_->listen(QHostAddress::LocalHost, DEFAULT_PORT)) {
        QTextStream out(stdout, QIODevice::WriteOnly);
        out << DEFAULT_PORT << "\n";
    } else {
        qCritical() << server_->errorString();
    }
}
