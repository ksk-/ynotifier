#pragma once

#include <QtCore/QAbstractListModel>

#include "Notification.h"

class NotificationModel final : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ rowCount NOTIFY rowCountChanged)

public:
    enum Roles {
        TitleRole = Qt::UserRole + 1,
        ContentRole,
    };

public:
    explicit NotificationModel(QObject* parent = nullptr);
    ~NotificationModel() override;

    QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

    void add(Notification* notication);

    template<typename... Ts>
    void add(Ts&&... args)
    {
        add(new Notification(std::forward<Ts>(args)...));
    }

signals:
    void rowCountChanged();

public slots:
    void remove(int row);

private:
    Q_DISABLE_COPY(NotificationModel)

private:
    QList<Notification*> notifications_;
};
