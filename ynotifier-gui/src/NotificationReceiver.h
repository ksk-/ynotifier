#pragma once

#include <QtCore/QObject>

class NotificationModel;
class QTcpServer;

class NotificationReceiver final : public QObject
{
    Q_OBJECT

public:
    explicit NotificationReceiver(NotificationModel& model, QObject* parent = nullptr);

    void start();

private:
    Q_DISABLE_COPY(NotificationReceiver)

private:
    NotificationModel& model_;
    QTcpServer* server_;
};
