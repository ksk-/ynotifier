#pragma once

#include <QtCore/QJsonObject>

class Notification final
{
public:
    Notification(QString title, QString content);
    Notification(const QJsonObject& notification);

    QString title() const;
    QString content() const;

private:
    QString title_;
    QString content_;
};
