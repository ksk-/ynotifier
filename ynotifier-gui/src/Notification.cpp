#include "Notification.h"

Notification::Notification(QString title, QString content)
    : title_(std::move(title))
    , content_(std::move(content))
{}

Notification::Notification(const QJsonObject& notification)
    : Notification(notification["title"].toString(), notification["content"].toString())
{}

QString Notification::title() const
{
    return title_;
}

QString Notification::content() const
{
    return content_;
}
