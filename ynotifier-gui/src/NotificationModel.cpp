#include <QtCore/QTimer>

#include "NotificationModel.h"

constexpr auto NOTIFICATION_LIFTIME = 5 * 1000;

NotificationModel::NotificationModel(QObject* parent)
    : QAbstractListModel(parent)
{}

NotificationModel::~NotificationModel()
{
    qDeleteAll(notifications_);
}

QHash<int, QByteArray> NotificationModel::roleNames() const
{
    static const QHash<int, QByteArray> roles = {
        {TitleRole, "title"},
        {ContentRole, "content"},
    };

    return roles;
}

int NotificationModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return notifications_.size();
}

QVariant NotificationModel::data(const QModelIndex& index, int role) const
{
    QVariant result;

    const Notification* notification = notifications_[index.row()];
    Q_CHECK_PTR(notification);

    switch (role) {
        case TitleRole:
            result = notification->title();
            break;

        case ContentRole:
            result = notification->content();
            break;

        default:
            break;
    }

    return result;
}

void NotificationModel::add(Notification* notication)
{
    beginInsertRows(QModelIndex(), notifications_.size(), notifications_.size());
    notifications_.push_back(notication);
    endInsertRows();

    emit rowCountChanged();

    QTimer::singleShot(NOTIFICATION_LIFTIME, [this, notication] {
        remove(notifications_.indexOf(notication));
    });
}

void NotificationModel::remove(int row)
{
    if ((row >= 0) && (row < rowCount())) {
        beginRemoveRows(QModelIndex(), row, row);
        delete notifications_.takeAt(row);
        endRemoveRows();

        emit rowCountChanged();
    }
}
