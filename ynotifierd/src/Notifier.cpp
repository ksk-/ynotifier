#include <QtCore/QDataStream>
#include <QtCore/QJsonDocument>
#include <QtCore/QProcess>

#include <QtNetwork/QHostAddress>
#include <QtNetwork/QTcpSocket>

#include "Notifier.h"

namespace
{
    QString readLine(QIODevice& device, int timeout = 30 * 1000)
    {
        Q_ASSERT(device.isOpen() && device.isReadable());

        return ((device.canReadLine() || (device.waitForReadyRead(timeout) && device.canReadLine())) || !device.atEnd())
                   ? QString::fromUtf8(device.readLine()).trimmed()
                   : QString();
    }
}

Notifier::Notifier(QObject* parent)
    : QObject(parent)
    , process_(new QProcess(this))
    , socket_(new QTcpSocket(this))
{
    process_->setReadChannel(QProcess::StandardOutput);
}

void Notifier::sendNotification(const QByteArray& notification)
{
    Q_ASSERT(socket_->isOpen() && socket_->isWritable());

    QDataStream out(socket_);
    out << notification.toBase64();
    out << "\n";

    socket_->flush();
}

void Notifier::sendNotification(const QJsonObject& notification)
{
    sendNotification(QJsonDocument(notification).toJson());
}

void Notifier::start(const QString& filename)
{
    if (process_->state() == QProcess::NotRunning) {
        process_->start(filename);

        if (process_->waitForStarted() && process_->waitForReadyRead()) {
            socket_->connectToHost(QHostAddress::LocalHost, readLine(*process_).toUShort());
            socket_->waitForConnected();
        } else {
            qCritical() << process_->errorString() << filename;
        }
    }
}

void Notifier::stop()
{
    process_->close();
}
