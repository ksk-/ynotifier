#pragma once

#include <QtCore/QObject>

class Notifier;
class QUdpSocket;

class NotificationService final : public QObject
{
    Q_OBJECT

    struct Settings final
    {
        QString notifier;
        quint16 port;
    };

public:
    explicit NotificationService(QObject* parent = nullptr);

    void configure(const QString& path);

public slots:
    void start();
    void stop();

private:
    Q_DISABLE_COPY(NotificationService)

    void processDatagram(QByteArray payload);


private:
    Settings settings_;

    Notifier* notifier_;
    QUdpSocket* socket_;
};
