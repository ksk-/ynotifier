#include <array>

#include "Service.h"

#ifdef Q_OS_WIN
#include "ServicePrivate_windows.h"
#else
#include "ServicePrivate_posix.h"
#endif

Service &Service::instance()
{
    if (!instance_) {
        instance_.reset(new Service);
    }

    return *instance_;
}

Service& Service::setServiceName(std::string_view serviceName)
{
    ServicePrivate::instance().setServiceName(serviceName);
    return *this;
}

Service& Service::setDescription(std::string_view description)
{
    ServicePrivate::instance().setDescription(description);
    return *this;
}

Service& Service::onStarted(std::function<int()> callback)
{
    ServicePrivate::instance().onStarted(std::move(callback));
    return *this;
}

Service &Service::onStopped(std::function<void()> callback)
{
    ServicePrivate::instance().onStopped(std::move(callback));
    return *this;
}

Service& Service::setMode(Mode mode)
{
    ServicePrivate::instance().setMode(mode);
    return *this;
}

int Service::exec()
{
    return ServicePrivate::instance().exec();
}
