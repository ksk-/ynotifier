#pragma once

#include <QtCore/QtGlobal>

#include <functional>
#include <memory>
#include <string_view>

class Service final
{
public:
    enum class Mode : quint8 {
        ConsoleApplication,
        Daemon,
    };

public:
    static Service& instance();

    Service& setServiceName(std::string_view serviceName);
    Service& setDescription(std::string_view description);

    Service& onStarted(std::function<int()> callback);
    Service& onStopped(std::function<void()> callback);

    Service& setMode(Mode mode);

    int exec();

private:
    Q_DISABLE_COPY(Service)

    Service() = default;

private:
    static inline std::unique_ptr<Service> instance_;
};
