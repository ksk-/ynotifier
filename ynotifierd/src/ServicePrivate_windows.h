#pragma once

#include <QtCore/QtGlobal>

#include <functional>
#include <future>
#include <memory>

#include <Windows.h>

#include "Service.h"

class ServicePrivate_windows final
{
    friend void WINAPI serviceCtrlHandler(DWORD ctrl);
    friend void WINAPI serviceMain(DWORD argc, LPTSTR* argv);

public:
    static ServicePrivate_windows &instance();

    void setServiceName(std::string_view serviceName);
    void setDescription(std::string_view description);

    void onStarted(std::function<int()> callback);
    void onStopped(std::function<void()> callback);

    void setMode(Service::Mode mode);

    int exec();

private:
    Q_DISABLE_COPY(ServicePrivate_windows)

    ServicePrivate_windows();

    void start();
    void stop();

    void reportServiceStatus(DWORD currentState, DWORD exitCode, DWORD waitHint);

private:
    SERVICE_STATUS status_;
    SERVICE_STATUS_HANDLE statusHandle_;

    std::future<int> serviceTask_;

    std::string serviceName_;
    std::string description_;

    std::function<int()> onStarted_;
    std::function<void()> onStopped_;

    Service::Mode mode_;

    static inline std::unique_ptr<ServicePrivate_windows> instance_;
};

using ServicePrivate = ServicePrivate_windows;
