#include <array>

#include "ServicePrivate_windows.h"

namespace
{
    int runService(const std::string& serviceName, LPSERVICE_MAIN_FUNCTION serviceMain)
    {
        std::array<SERVICE_TABLE_ENTRY, 2> services;
        services[0] = { const_cast<char *>(serviceName.data()), serviceMain };
        services[1] = { nullptr, nullptr };

        return (StartServiceCtrlDispatcher(services.data()) == TRUE) ? 0 : static_cast<int>(GetLastError());
    }

    void setServiceDescription(const std::string& serviceName, const std::string& description)
    {
        if (SC_HANDLE scManager = OpenSCManager(nullptr, nullptr, SC_MANAGER_ALL_ACCESS); scManager) {
            if (SC_HANDLE scService = OpenService(scManager, serviceName.data(), GENERIC_WRITE); scService) {
                SERVICE_DESCRIPTION serviceDescription = { const_cast<char *>(description.data()) };
                ChangeServiceConfig2(scService, SERVICE_CONFIG_DESCRIPTION, &serviceDescription);
            }
        }
    }
}

void WINAPI serviceCtrlHandler(DWORD ctrl)
{
    auto& service = ServicePrivate_windows::instance();

    switch (ctrl) {
        case SERVICE_CONTROL_STOP:
            service.stop();
            break;

        default:
            break;
    }
}

void WINAPI serviceMain([[maybe_unused]] DWORD argc, [[maybe_unused]] LPTSTR* argv)
{
    ServicePrivate_windows::instance().start();
}

ServicePrivate_windows& ServicePrivate_windows::instance()
{
    if (!instance_) {
        instance_.reset(new ServicePrivate_windows);
    }

    return *instance_;
}

ServicePrivate_windows::ServicePrivate_windows()
    : statusHandle_(nullptr)
{}

void ServicePrivate_windows::start()
{
    statusHandle_ = RegisterServiceCtrlHandler(serviceName_.data(), serviceCtrlHandler);

    if (statusHandle_) {
        setServiceDescription(serviceName_, description_);

        status_.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
        status_.dwServiceSpecificExitCode = 0;

        reportServiceStatus(SERVICE_START_PENDING, NO_ERROR, 3000);
        serviceTask_ = std::async(onStarted_);
        reportServiceStatus(SERVICE_RUNNING, NO_ERROR, 0);
    }
}

void ServicePrivate_windows::stop()
{
    reportServiceStatus(SERVICE_STOP_PENDING, NO_ERROR, 0);

    onStopped_();
    serviceTask_.wait();

    reportServiceStatus(SERVICE_STOPPED, NO_ERROR, 0);
}

void ServicePrivate_windows::reportServiceStatus(DWORD currentState, DWORD exitCode, DWORD waitHint)
{
    static DWORD checkPoint = 1;

    status_.dwCurrentState = currentState;
    status_.dwWin32ExitCode = exitCode;
    status_.dwWaitHint = waitHint;
    status_.dwControlsAccepted = (currentState == SERVICE_START_PENDING) ? 0 : SERVICE_ACCEPT_STOP;
    status_.dwCheckPoint = ((currentState == SERVICE_RUNNING) || (currentState == SERVICE_STOPPED)) ? 0 : checkPoint++;

    SetServiceStatus(statusHandle_, &status_);
}

void ServicePrivate_windows::setServiceName(std::string_view serviceName)
{
    serviceName_ = serviceName;
}

void ServicePrivate_windows::setDescription(std::string_view description)
{
    description_ = description;
}

void ServicePrivate_windows::onStarted(std::function<int()> callback)
{
    onStarted_ = std::move(callback);
}

void ServicePrivate_windows::onStopped(std::function<void()> callback)
{
    onStopped_ = std::move(callback);
}

void ServicePrivate_windows::setMode(Service::Mode mode)
{
    mode_ = mode;
}

int ServicePrivate_windows::exec()
{
    return (mode_ == Service::Mode::Daemon) ? runService(serviceName_, serviceMain) : onStarted_();
}
