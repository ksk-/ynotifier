#include "ServicePrivate_posix.h"

ServicePrivate_posix& ServicePrivate_posix::instance()
{
    if (!instance_) {
        instance_.reset(new ServicePrivate_posix);
    }

    return *instance_;
}

void ServicePrivate_posix::setServiceName([[maybe_unused]] std::string_view serviceName) {}
void ServicePrivate_posix::setDescription([[maybe_unused]] std::string_view description) {}

void ServicePrivate_posix::onStarted(std::function<int()> callback)
{
    onStarted_ = std::move(callback);
}

void ServicePrivate_posix::onStopped(std::function<void()> callback)
{
    onStopped_ = std::move(callback);
}

void ServicePrivate_posix::setMode(Service::Mode mode)
{
    mode_ = mode;
}

int ServicePrivate_posix::exec()
{
    return onStarted_();
}
