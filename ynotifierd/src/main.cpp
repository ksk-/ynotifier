#include <QtCore/QCoreApplication>
#include <QtCore/QCommandLineParser>

#include "NotificationService.h"

#include "Service.h"

constexpr auto DEFAULT_CONFIG_FILE = "ynotifierd.conf";

int main(int argc, char** argv)
{
    auto app = std::make_shared<QCoreApplication>(argc, argv);
    app->setApplicationName(APPLICATION_NAME);

    const QCommandLineOption noDaemonOption({ "e", "no-daemon" }, QStringLiteral("Runs as a regular application."));

    QCommandLineParser parser;
    parser.setApplicationDescription(APPLICATION_DESCRIPTION);
    parser.addOption(noDaemonOption);
    parser.addHelpOption();
    parser.process(*app);

    Service& service = Service::instance()
        .setServiceName(APPLICATION_NAME)
        .setDescription(APPLICATION_DESCRIPTION)
        .onStarted([app] {
            NotificationService service;
            service.configure(DEFAULT_CONFIG_FILE);
            service.start();

            QObject::connect(app.get(), &QCoreApplication::aboutToQuit, &service, &NotificationService::stop);

            return app->exec();
        })
        .onStopped(&QCoreApplication::quit)
        .setMode(parser.isSet(noDaemonOption) ? Service::Mode::ConsoleApplication : Service::Mode::Daemon);

    return service.exec();
}
