#pragma once

#include <QtCore/QObject>

class QProcess;
class QTcpSocket;

class Notifier final : public QObject
{
    Q_OBJECT

public:
    explicit Notifier(QObject* parent = nullptr);

    bool isRunning() const;

    void sendNotification(const QByteArray& notification);
    void sendNotification(const QJsonObject& notification);

public slots:
    void start(const QString& filename);
    void stop();

private:
    Q_DISABLE_COPY(Notifier)

private:
    QProcess* process_;
    QTcpSocket* socket_;
};
