#pragma once

#include <QtCore/QtGlobal>

#include <functional>
#include <memory>
#include <string_view>

class ServicePrivate_posix final
{
public:
    static ServicePrivate_posix &instance();

    void setServiceName(std::string_view serviceName);
    void setDescription(std::string_view description);

    void onStarted(std::function<int()> callback);
    void onStopped(std::function<void()> callback);

    void setMode(Service::Mode mode);

    int exec();

private:
    Q_DISABLE_COPY(ServicePrivate_posix)

    ServicePrivate_posix() = default;

private:
    std::function<int()> onStarted_;
    std::function<void()> onStopped_;

    Service::Mode mode_;

    static inline std::unique_ptr<ServicePrivate_posix> instance_;
};

using ServicePrivate = ServicePrivate_posix;
