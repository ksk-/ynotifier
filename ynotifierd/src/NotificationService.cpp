#include <QtCore/QBuffer>
#include <QtCore/QFile>
#include <QtCore/QJsonDocument>

#include <QtNetwork/QNetworkDatagram>
#include <QtNetwork/QUdpSocket>

#include "Notifier.h"

#include "NotificationService.h"

namespace
{
    QByteArray readFile(const QString& path)
    {
        QFile file(path);
        return file.open(QIODevice::ReadOnly) ? file.readAll() : QByteArray();
    }
}

NotificationService::NotificationService(QObject* parent)
    : QObject(parent)
    , notifier_(new Notifier(this))
    , socket_(new QUdpSocket(this))
{
    connect(socket_, &QIODevice::readyRead, [this] {
        while ((socket_->state() == QUdpSocket::BoundState) && socket_->hasPendingDatagrams()) {
            if (const QNetworkDatagram datagram = socket_->receiveDatagram(); datagram.isValid()) {
                processDatagram(datagram.data());
            } else {
                qCritical() << "Invalid datagram from" << datagram.senderPort();
            }
        }
    });
}

void NotificationService::configure(const QString& path)
{
    const QVariantMap settings = QJsonDocument::fromJson(readFile(path)).toVariant().toMap();

    if (!settings.isEmpty()) {
        settings_.notifier = qvariant_cast<QString>(settings["notifier"]);
        settings_.port = qvariant_cast<quint16>(settings["port"]);
    } else {
        qCritical() << "Invalid configuration" << path;
    }
}

void NotificationService::start()
{
    if (!socket_->bind(QHostAddress::LocalHost, settings_.port)) {
        qCritical() << socket_->errorString();
    }
}

void NotificationService::stop()
{
    notifier_->stop();
    socket_->close();
}

void NotificationService::processDatagram(QByteArray payload)
{
    QByteArray data = std::move(payload);

    QBuffer buffer(&data);
    buffer.open(QIODevice::ReadOnly);

    if (buffer.canReadLine()) {
        const QString message = QString::fromUtf8(buffer.readLine()).trimmed();

        if (message == "NOTIFICATION") {
            notifier_->start(settings_.notifier);
            notifier_->sendNotification(buffer.readAll());
        } else {
            qWarning() << "Unknown message" << message;
        }
    } else {
        qWarning() << "Can't read the message";
    }
}
