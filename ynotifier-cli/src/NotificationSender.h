#pragma once

#include <QtCore/QObject>

#include <QtNetwork/QHostAddress>

class NotificationSender final : public QObject
{
    Q_OBJECT

public:
    explicit NotificationSender(QObject* parent = nullptr);

    void setHost(const QHostAddress& host);
    void setPort(quint16 port);

signals:
    void sent();

public slots:
    void send(const QString& title, const QString& content);

private:
    Q_DISABLE_COPY(NotificationSender)

private:
    QHostAddress host_;
    quint16 port_;
};
