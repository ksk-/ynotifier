#include <QtCore/QCoreApplication>
#include <QtCore/QCommandLineParser>
#include <QtCore/QTimer>

#include "NotificationSender.h"

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);
    app.setApplicationName(APPLICATION_NAME);

    const QCommandLineOption hostOption(
        "host", QStringLiteral("Specifies the ynotifierd hostname or IP-address."), "host"
    );

    const QCommandLineOption portOption(
        { "p", "port" }, QStringLiteral("Specifies the ynotifierd port number."), "port"
    );

    QCommandLineParser parser;
    parser.setApplicationDescription(APPLICATION_DESCRIPTION);
    parser.addOption(hostOption);
    parser.addOption(portOption);
    parser.addHelpOption();
    parser.process(app);

    NotificationSender sender;

    if (parser.isSet(hostOption)) {
        sender.setHost(QHostAddress(parser.value(hostOption)));
    }

    if (parser.isSet(portOption)) {
        sender.setPort(parser.value(portOption).toUShort());
    }

    QObject::connect(&sender, &NotificationSender::sent, &app, &QCoreApplication::quit);

    QTimer::singleShot(0, &sender, [&sender] {
        sender.send("Title", "Body");
    });

    return app.exec();
}
