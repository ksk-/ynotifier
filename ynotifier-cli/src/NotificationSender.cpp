#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QTextStream>

#include <QtNetwork/QUdpSocket>

#include "NotificationSender.h"

constexpr auto DEFAULT_PORT = 3333;

namespace
{
    QString notification(const QString& title, const QString& content)
    {
        const QJsonObject json = {
            { "title", title },
            { "content", content },
        };

        return QJsonDocument(json).toJson();
    }
}

NotificationSender::NotificationSender(QObject* parent)
    : QObject(parent)
    , host_(QHostAddress::LocalHost)
    , port_(DEFAULT_PORT)
{}

void NotificationSender::setHost(const QHostAddress& host)
{
    host_ = host;
}

void NotificationSender::setPort(quint16 port)
{
    port_ = port;
}

void NotificationSender::send(const QString& title, const QString& content)
{
    QByteArray data;

    QTextStream out(&data, QIODevice::WriteOnly);
    out << "NOTIFICATION" << "\n";
    out << notification(title, content);
    out.flush();

    QUdpSocket socket;
    socket.writeDatagram(data, host_, port_);

    emit sent();
}
